import {Laptop, laptopArray} from "./laptop.js";

export class Bank {

    constructor (id,name,balance,loan){
        this.id=id
        this.name=name
        this.balance=balance
        this.loan= loan
        this.eligibleLoan=true
    }

    newLoan(loanAmount){
        if(loanAmount>this.balance*2 || !this.eligibleLoan || this.loan>0){
            return false
        }else{
            this.setLoan(loanAmount)
            this.eligibleLoan=false
            return true
        }

    }
    setLoan(loan){
        this.loan=loan
    }
    getLoan(){
        return this.loan
    }
    setBalance(balanceAmount){
        this.balance=balanceAmount
    }
    getBalance(){
        return this.balance
    }

    buyLaptop(Laptop){
        if(this.getBalance()>Laptop.getPrice()){
            this.setBalance(this.getBalance()-Laptop.getPrice())

            alert('Congratulations you bought a laptop')
        }else{
            alert('Are you sure you have enough to buy this laptop? Contact customer service if this error should not occur')
        }

        this.eligibleLoan=true
    }

}

export const bank= new Bank("1","Joe Banker",4000,0,true)
