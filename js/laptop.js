export class Laptop {

    constructor (id,name,price,description,features,url){
        this.id=id
        this.name=name
        this.price=price
        this.description=description
        this.features=features
        this.url=url
    }

    getPrice(){
        return this.price
    }
}

 export const laptopArray=[
new Laptop(1,'Acer Nitro',10000,'Perfect for losing in Warzone and loads the Gulag quickly.' +
    'It does not have enough space to store the full game, but we sell additional hard drives.',['Highlighted AWSD-keys so people ' +
    'know you are a pro gamer',' Sharp edges and very much red.'],'img/acernitro.jpg'),

new Laptop(2, 'Corporate inc 10', 6500, 'Perfect for companies, ' +
    'can have one or maximum two chrome tabs open',['Big powerful fan',' Eco friendly.'],'img/corporate.jpg'),

new Laptop(3, 'Home LX5', 5000, 'Perfect for home use, ' +
    'starts with decent performance... great option to buy insurance in store',
    ['Pre installed software trials',' HD-ready.'],'img/home.jpg'),

new Laptop(4, 'Kebnak-1', 40000, '40 copies already sold, ' +
    'Original price was 6300,- which equates to 40000,- today, no screen needed comes with in built lights as output',
    ['256 bytes storage',' 1 MHz processing.'],'img/kebnak.jpg')

]

