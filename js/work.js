
export class Work {

    constructor (id,name,pay,hourRate,tax) {
        this.id = id
        this.name = name
        this.pay = pay
        this.hourRate=hourRate
        this.tax=tax
    }
    setPay(pay){
        this.pay=pay
    }
    getPay(){
        return this.pay
    }
    getTax(){
        return this.tax
    }
    getPayTax(){
        return this.pay*this.tax
    }

    transfer(Bank){
        let deducted=0
        if (Bank.loan>this.getPayTax()){
           deducted=this.getPayTax()
            Bank.setLoan(Bank.getLoan()-deducted)
        }else {
            while (Bank.getLoan()> 0) {
                Bank.setLoan(Bank.getLoan()-1)
                deducted++
            }
        }
        Bank.setBalance(this.getPay()-deducted+Bank.getBalance())
        this.setPay(0)


    }

    work(){
        this.setPay(this.getPay()+this.hourRate)
    }

    payLoan(Bank){

        if (Bank.getLoan()>this.getPay()){
            Bank.setLoan(Bank.getLoan()-this.getPay())
            this.setPay(0)
        }else{
            while(Bank.getLoan()>0){
                Bank.setLoan(Bank.getLoan()-1)
                this.setPay(this.getPay()-1)
            }
        }
    }




}

export const work=new Work(1,"Joe Banker",2000,100,0.1)