import {Laptop, laptopArray} from "./js/laptop.js";
import {bank} from "./js/bank.js";
import {work} from "./js/work.js";



const elLaptopChosen=document.getElementById('laptop-selection')
const elLaptopDisplay=document.getElementById('laptop-picture')
const elTransferToBank=document.getElementById("bank-button")
const elWork=document.getElementById("work-button")
const elNewLoan=document.getElementById("get-loan-button")
const elLoanmessage=document.getElementById('loan-text')
const elLoanAmmount=document.getElementById('loan-amount')
const elPayAmount=document.getElementById("pay-amount")
const elBalanceAmount=document.getElementById("balance")
const elFeature=document.getElementById("features")
const elDescription=document.getElementById("description")
const elLaptopAndDes=document.getElementById("description-picture")
const elBuyLaptopButton=document.getElementById("buy-laptop")
const elBuyLaptopPrice=document.getElementById("price-laptop")
const elRepayButton=document.getElementById("repay-loan")

let laptopImg=document.createElement('img')

elPayAmount.innerText=work.getPay()
elBalanceAmount.innerText=bank.getBalance()
elLaptopAndDes.style.visibility='hidden'
elRepayButton.style.visibility='hidden'





for(const laptop of laptopArray){
    const laptopOption=document.createElement('option')
    laptopOption.value=laptop.id
    laptopOption.innerText=laptop.name
    elLaptopChosen.appendChild(laptopOption)
}



elLaptopChosen.addEventListener('change',function(){
    if (this.value==-1){
        elLaptopAndDes.style.visibility='hidden'

    }else{

        laptopImg.width=256
        laptopImg.src=laptopArray[this.value-1].url
        elLaptopDisplay.appendChild(laptopImg)
        elFeature.innerText=laptopArray[this.value-1].features
        elDescription.innerText=laptopArray[this.value-1].description
        elLaptopAndDes.style.visibility='visible'
        elBuyLaptopPrice.innerText=laptopArray[this.value-1].price




    }

})

elTransferToBank.addEventListener('click', function(){
    work.transfer(bank)
    elBalanceAmount.innerText=bank.getBalance()
    elPayAmount.innerText=work.getPay()
    elLoanAmmount.innerText=bank.getLoan()
})

elWork.addEventListener('click', function(){
    work.work()
    elPayAmount.innerText=work.getPay()

})

elNewLoan.addEventListener('click', function(){
    let amount=prompt('please enter requested loan amount: ')
    if(bank.newLoan(amount)){
        elLoanmessage.style.visibility='visible'
        elLoanAmmount.style.visibility='visible'
        elRepayButton.style.visibility='visible'

        elLoanAmmount.innerText=amount

    }
})

elBuyLaptopButton.addEventListener('click', function(){
    bank.buyLaptop(laptopArray[elLaptopChosen.value -1])
    elBalanceAmount.innerText=bank.getBalance()

})


elRepayButton.addEventListener('click', function(){
    work.payLoan(bank)
    elPayAmount.innerText=work.getPay()
    elLoanAmmount.innerText=bank.getLoan()




})